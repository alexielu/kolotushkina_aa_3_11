﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cards.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Cards
{
    enum GameState : byte
    {
        SetStartHand = 0,
        TakeCard = 1,
        PlayerMoves = 2,
        ChangingPlayer = 3
    }
    
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Camera _cam;
        
        private Material _baseMaterial;
        private List<CardPropertiesData> _allCards;
        [SerializeField] private Card[] _player1Deck;
        private Card[] _player2Deck;

        [SerializeField] private CardPackConfiguration[] _packs;
        [Space, SerializeField] private int _deckSize;
        [SerializeField] private Card _cardPrefab;

        [SerializeField, Range(1, 10)] private int _cardsInPreview;
        [SerializeField] private Previewer _previewer;

        [Space, Header("FIRST PLAYER")]
        [SerializeField] private SideType _character1Type;
        [SerializeField] private Character _character1;
        [SerializeField] private PlayerHand _player1Hand;
        [SerializeField] private Transform _deck1Holder;
        [SerializeField] private TableReplace _player1Replacer;
        [SerializeField] private int[] firstPlayerDeckIDs;
        private int _player1DmgCounter = 1;
        
        [Space, Header("SECOND PLAYER")]
        [SerializeField] private SideType _character2Type;
        [SerializeField] private Character _character2;
        [SerializeField] private PlayerHand _player2Hand;
        [SerializeField] private Transform _deck2Holder;
        [SerializeField] private TableReplace _player2Replacer;
        [SerializeField] private int[] secondPlayerDeckIDs;
        private int _player2DmgCounter = 1;

        [SerializeField] private Button _readyButton;
        
        private bool _firstPlayerTurn;
        private GameState _state;
        private int _roundsCounter;
        private int _cardReturnsCounter;

        public static GameManager Instance;
        
        private CardPropertiesData FindCardInPacksById(int id)
        {
            CardPropertiesData card;
            
            foreach (var pack in _packs)
            {
                card = pack.TryGetCardById(id);
                if (card.Id == id)
                    return card;
            }

            return new CardPropertiesData();
        }
        private Card[] CreateDeck(Transform deckHolder, bool firstPlayer)
        {
            var arrayToCheck = firstPlayer ? firstPlayerDeckIDs : secondPlayerDeckIDs;

            var deck = new Card[_deckSize];
            var vector = Vector3.zero;

            for (int i = 0; i < _deckSize; i++)
            {
                deck[i] = Instantiate(_cardPrefab, deckHolder);
                deck[i].transform.localPosition = vector;
                deck[i].SwitchEnable();
                vector += new Vector3(0f, 0.01f, 0f);

                CardPropertiesData randomCard;
                if (arrayToCheck.Length - 1 >= i && arrayToCheck[i] != 0) // Попробуем взять заданную карту по ID
                {
                    randomCard = FindCardInPacksById(arrayToCheck[i]);
                    
                    // Если такого айди не существует в паках, берем рандомную карту
                    if (randomCard.Id != arrayToCheck[i]) 
                        randomCard = _allCards[Random.Range(0, _allCards.Count - 1)];
                }
                else // если колода не собрана до конца, добираем рандомных карт
                {
                    randomCard = _allCards[Random.Range(0, _allCards.Count - 1)];
                }

                var newMat = new Material(_baseMaterial){mainTexture =  randomCard.Texture};
                deck[i].SetConfiguration(randomCard, newMat, CardUtility.GetDescriptionById(randomCard.Id));
            }

            return deck;
        }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
            
            IEnumerable<CardPropertiesData> arrayCards = new List<CardPropertiesData>();

            foreach (var pack in _packs)
                arrayCards = pack.UnionProperties(arrayCards);

            _baseMaterial = new Material(Shader.Find("TextMeshPro/Sprite"));
            _baseMaterial.renderQueue = 2995;

            _allCards = new List<CardPropertiesData>(arrayCards);
            _firstPlayerTurn = true;

            _state = GameState.SetStartHand;
        }

        void Start()
        {
            _roundsCounter = 1;
            
            _player1Deck = CreateDeck(_deck1Holder, true);
            ShuffleDeck(_player1Deck);
            _character1.RefreshMana(_roundsCounter);
            
            _player2Deck = CreateDeck(_deck2Holder, false);
            ShuffleDeck(_player2Deck);
            _character1.RefreshMana(_roundsCounter);

            _previewer.PreparePlacer(_cardsInPreview);
            _previewer.SetCards(_player1Deck);
            _readyButton.gameObject.SetActive(true);
        }

        private void ShuffleDeck(Card[] deck)
        {
            for (int i = 0; i < deck.Length; i++)
            {
                var cardToRemember = deck[i];
                var rndIndex = Random.Range(i, deck.Length);
                
                deck[i] = deck[rndIndex];
                deck[rndIndex] = cardToRemember;
            }
        }
        public void ReturnCardToDeck(Card card)
        {
            _previewer.ChangeCard(card, _firstPlayerTurn ? _deck1Holder : _deck2Holder);
        }
        public void SetPreviewCardsToHand()
        {
            var cards = _previewer.PreviewCards;
            foreach (var card in cards)
            {
                TakeCardFromPreview(card);
            }
            
            _readyButton.gameObject.SetActive(false);
            ShuffleDeck(_firstPlayerTurn ? _player1Deck : _player2Deck);
            _state = GameState.PlayerMoves;
        }
        private void TakeCardFromPreview(Card card)
        {
            var hand = _firstPlayerTurn ? _player1Hand : _player2Hand;
            var deck = _firstPlayerTurn ? _player1Deck : _player2Deck;

            for (int i = 0; i < deck.Length; i++)
            {
                if (deck[i] == card)
                    deck[i] = null;
            }

            hand.TrySetNewCard(card, false);
        }
        public void TryToTakeCard(Card card)
        {
            if (_state != GameState.TakeCard) return;
            
            var deck = _firstPlayerTurn ? _player1Deck : _player2Deck;
            if (!deck.Contains(card)) return; // Не даем брать карту из колоды по тыку по вражеской колоде
            
            var hand = _firstPlayerTurn ? _player1Hand : _player2Hand;
                
            var index = deck.Length - 1;
            for (int i = 0; i < deck.Length; i++)
            {
                if (deck[i] != null)
                {
                    index = i;
                    continue;
                }
            }

            if (deck[index] != null)
                hand.TrySetNewCard(deck[index], true);
            else
            {
                DamagePlayer();
            }
                
            deck[index] = null;
            _state = GameState.PlayerMoves;
        }
        public bool TryToPutCardOnTable(Card card)
        {
            var player = _firstPlayerTurn ? _character1 : _character2;
            var replacer = _firstPlayerTurn ? _player1Replacer : _player2Replacer;
            var hand = _firstPlayerTurn ? _player1Hand : _player2Hand;

            bool success = replacer.TrySetCard(card);
            if (success)
            {
                var enoughMana = player.TrySpendMana(card.Cost);
                if (enoughMana)
                    hand.RemoveCard(card);

                return enoughMana;
            }
            
            return success;
        }
        
        private void DamagePlayer()
        {
            if (_firstPlayerTurn)
            {
                _character1.TakeDamage(_player1DmgCounter);
                _player1DmgCounter++;
            }
            else
            {
                _character2.TakeDamage(_player2DmgCounter);
                _player2DmgCounter++;
            }
        }

        public void ChangePlayer()
        {
            _state = GameState.ChangingPlayer;
            
            StartCoroutine(CameraRotation());
            _character1.AnimateRotation(2f);
            _character2.AnimateRotation(2f);
            _player1Replacer.AnimateRotation(2f);
            _player2Replacer.AnimateRotation(2f);
            
            _firstPlayerTurn = !_firstPlayerTurn;

            if (_firstPlayerTurn)
            {
                if (_roundsCounter < 10) _roundsCounter++;
                _character1.RefreshMana(_roundsCounter);
            }
            else
            {
                _character2.RefreshMana(_roundsCounter);
                if (_roundsCounter == 1)
                {
                    _previewer.transform.Rotate(Vector3.up, 180f);
                    _previewer.SetCards(_player2Deck);
                    _readyButton.gameObject.SetActive(true);
                }
            }
        }

        public void EndGame()
        {
            Debug.Log("End Game");
        }
        
        private IEnumerator CameraRotation()
        {
            var angleStart = _cam.transform.eulerAngles;
            var angleMid = angleStart + new Vector3(0f,0f,90f);
            var angleEnd = angleStart + new Vector3(0f,0f,180f);

            var cardHeight = 0f;

            var cardsInHands = new List<Card>();
            foreach (var card in _player1Hand.CardsInHand)
            {
                if (card != null)
                    cardsInHands.Add(card);
            }
            foreach (var card in _player2Hand.CardsInHand)
            {
                if (card != null)
                    cardsInHands.Add(card);
            }
            
            var time = 0f;
            while (time < 1f)
            {
                _cam.transform.eulerAngles = Vector3.Lerp(angleStart, angleMid, time);
                cardHeight = Mathf.Lerp(0f, .5f, time);
                
                foreach (var card in cardsInHands)
                {
                    var startPos = card.transform.localPosition;
                    card.transform.localPosition = new Vector3(startPos.x, cardHeight, startPos.z);
                }
                
                time += Time.deltaTime;
                yield return null;
            }
            
            foreach (var card in cardsInHands)
                card.AnimateRotation();
            
            time = 0f;
            while (time < 1f)
            {
                _cam.transform.eulerAngles = Vector3.Lerp(angleMid, angleEnd, time);
                cardHeight = Mathf.Lerp(.5f, 0f, time);
                foreach (var card in cardsInHands)
                {
                    card.transform.localPosition =
                        new Vector3(card.transform.localPosition.x, cardHeight, card.transform.localPosition.z);
                }
                
                time += Time.deltaTime;
                yield return null;
            }
            _cam.transform.eulerAngles = angleEnd;
            
            _state = GameState.TakeCard;
        }
    }
}