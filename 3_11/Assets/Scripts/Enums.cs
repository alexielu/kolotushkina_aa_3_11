﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
	public enum CardUnitType : byte
	{
		None = 0,
		Murloc = 1,
		Beast = 2,
		Elemental = 3,
		Mech = 4
	}

	public enum SideType : byte
	{
		Common = 0,
		Mage = 1,
		Warrior = 2
	}

	public enum CardState : byte
	{
		InPreview = 0,
		InDeck = 1,
		InHand = 2,
		OnTable = 3,
		Animated = 4
	}
}
