﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Cards
{
    public class Previewer : MonoBehaviour
    {
        [SerializeField] private float _spacing;
        private int _previewSize;
        [SerializeField] private float _movementTimer;
        private Card[] _cardsInPreview;
        private Vector3[] _positions;
        private Vector3[] _originalPositions; // массив позиций в колоде, куда возвращать

        private int _cardsCounter;
        private Card[] _currentDeck;

        private bool _nextMoveToHand = false;

        public Card[] PreviewCards => _cardsInPreview;

        public void PreparePlacer(int cardsAmount)
        {
            _previewSize = cardsAmount;
            _cardsInPreview = new Card[_previewSize];
            _positions = new Vector3[_previewSize];
            _originalPositions = new Vector3[_previewSize];
            GenerateCardsPositions();
        }
        
        private void GenerateCardsPositions()
        {
            var leftPointX = -(_positions.Length * .5f - .5f) * _spacing;
            for (int i = 0; i < _positions.Length; i++)
            {
                var currentX = leftPointX + _spacing * i;
                _positions[i] = new Vector3(currentX, 0f, 0f);
            }
        }

        public void SetCards(Card[] deck)
        {
            for (int i = 0; i < _previewSize; i++)
            {
                deck[i].Returnable = true;
                _cardsInPreview[i] = deck[i];
                StartCoroutine(MoveCardToPreview(deck[i], _positions[i], transform));
            }
            _cardsCounter = _previewSize;
            _currentDeck = deck;
        }

        public void ChangeCard(Card card, Transform deckHolder)
        {
            if (_cardsCounter >= _previewSize * 2) return;
            
            var returnableID = -1;
            for (int i = 0; i < _previewSize; i++)
            {
                if (_cardsInPreview[i] == card)
                {
                    returnableID = i;
                    break;
                }
            }

            if (returnableID == -1)
            {
                Debug.LogError("Trying return card, which is not in preview: " + card.name);
                return;
            }
            
            StartCoroutine(MoveCardToDeck(_cardsInPreview[returnableID], _originalPositions[returnableID], deckHolder));
            _cardsInPreview[returnableID] = _currentDeck[_cardsCounter];
            StartCoroutine(MoveCardToPreview(_currentDeck[_cardsCounter], _positions[returnableID], transform));
            _cardsCounter++;
        }
        
        private IEnumerator MoveCardToPreview(Card card, Vector3 endPos, Transform holder)
        {
            var time = 0f;
            card.transform.SetParent(holder);
            var startPos = card.transform.localPosition;

            card.AnimateRotation();
            while (time < _movementTimer)
            {
                card.transform.localPosition = Vector3.Lerp(startPos, endPos, time/_movementTimer);
                time += Time.deltaTime;
                yield return null;
            }

            card.transform.localPosition = endPos;
            card.State = CardState.InPreview;
        }
        
        private IEnumerator MoveCardToDeck(Card card, Vector3 endPos, Transform holder)
        {
            var time = 0f;
            card.transform.SetParent(holder);
            var startPos = card.transform.localPosition;

            card.AnimateRotation();
            while (time < _movementTimer)
            {
                card.transform.localPosition = Vector3.Lerp(startPos, endPos, time/_movementTimer);
                time += Time.deltaTime;
                yield return null;
            }

            card.transform.localPosition = endPos;
            card.State = CardState.InDeck;
        }
    }
}