﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Cards
{
    public class Character : MonoBehaviour
    {
        [SerializeField] private SideType _type;
        [SerializeField] private MeshRenderer _picture;
        [SerializeField] private TextMeshPro _name;
        [SerializeField] private TextMeshPro _healthText;
        [SerializeField] private TextMeshPro _manaText;

        private int _health;
        private int _mana;
        
        public void SetConfiguration(CharacterData data, Material picture)
        {
            _picture.material = picture; 
            _name.text = data.Name;
            _healthText.text = data.Health.ToString();
            _manaText.text = data.Mana.ToString();
            _type = data.Type;

            _health = data.Health;
            _mana = data.Mana;
        }

        public void TakeDamage(int damage)
        {
            _health -= damage;
            if (_health <= 0)
            {
                _health = 0;
                GameManager.Instance.EndGame();
            }

            _healthText.text = _health.ToString();
        }

        public bool TrySpendMana(int manaToSpend)
        {
            if (_mana < manaToSpend)
                return false;
            else
            {
                _mana -= manaToSpend;
                _manaText.text = _mana.ToString();
                return true;
            }
        }

        public void RefreshMana(int manaToSet)
        {
            _mana = manaToSet;
            _manaText.text = _mana.ToString();
        }

        public void AnimateRotation(float time)
        {
            StartCoroutine(RotatePortrait(time));
        }

        private IEnumerator RotatePortrait(float timer)
        {
            var startAngle = transform.localEulerAngles;
            var endAngle = new Vector3(startAngle.x, startAngle.y + 180f, startAngle.z);
            var time = 0f;

            while (time < timer)
            {
                transform.localEulerAngles = Vector3.Lerp(startAngle, endAngle, time / timer);
                time += Time.deltaTime;
                yield return null;
            }

            transform.localEulerAngles = endAngle;
        }
    }
}