﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class PlayerHand : MonoBehaviour
    {
        private Card[] _cardsInHand;
        private Vector3[] _positions;
        
        [Header("Hand Parameters")]
        [SerializeField] private float _spacing;
        [SerializeField] private int _handSize;

        [Header("Animation Parameters")]
        [SerializeField] private float _movementTimer;
        [SerializeField] private float _height;

        public Card[] CardsInHand => _cardsInHand;

        void Start()
        {
            _positions = new Vector3[_handSize];
            GenerateCardsPositions();
            
            _cardsInHand = new Card[_positions.Length];
        }
        
        private void GenerateCardsPositions()
        {
            var leftPointX = -(_positions.Length * .5f - .5f) * _spacing;
            for (int i = 0; i < _positions.Length; i++)
            {
                var currentX = leftPointX + _spacing * i;
                _positions[i] = new Vector3(currentX, 0f, 0f);
            }
        }

        public bool TrySetNewCard(Card newCard, bool needsRotation)
        {
            var res = GetLastPositionID();

            if (res == -1)
            {
                Destroy(newCard.gameObject);
                return false;
            }

            _cardsInHand[res] = newCard;
            StartCoroutine(MoveCardToHands(newCard, _positions[res], transform, needsRotation));
            return true;
        }

        public void RemoveCard(Card card)
        {
            for (int i = 0; i < _cardsInHand.Length; i++)
            {
                if (_cardsInHand[i] == card)
                {
                    _cardsInHand[i] = null;
                    break;
                }
            }
        }

        private int GetLastPositionID()
        {
            for (int i = 0; i < _cardsInHand.Length; i++)
            {
                if (_cardsInHand[i] == null)
                    return i;
            }

            return -1;
        }
        
        private IEnumerator MoveCardToHands(Card card, Vector3 endPos, Transform handHolder, bool needsRotation)
        {
            var time = 0f;
            card.transform.SetParent(handHolder);
            var startPos = card.transform.localPosition;

            if (needsRotation) card.AnimateRotation();
            
            while (time < _movementTimer)
            {
                var quaterTime = _movementTimer * .25f;
                var restTime = _movementTimer - quaterTime;

                var posXZ = Vector2.Lerp(new Vector2(startPos.x, startPos.z), new Vector2(endPos.x, endPos.z), time/_movementTimer);

                float posY;
                if (time < quaterTime)
                    posY = Mathf.Lerp(0f, _height, time / quaterTime);
                else
                    posY = Mathf.Lerp(_height, 0f, (time - quaterTime)/restTime);
                
                card.transform.localPosition = new Vector3(posXZ.x, posY, posXZ.y);
                
                time += Time.deltaTime;
                yield return null;
            }

            card.transform.localPosition = endPos;
            card.State = CardState.InHand;
        }
    }
}