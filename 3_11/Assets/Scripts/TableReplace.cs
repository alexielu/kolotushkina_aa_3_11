﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Cards
{
    public class TableReplace : MonoBehaviour
    {
        private Card[] _cardsOnTable;
        private Vector3[] _positions;
        
        [Header("Placement Parameters")]
        [SerializeField] private float _spacing;
        [SerializeField] private int _tableSize;
        [SerializeField] private int _movementTimer;

        void Start()
        {
            _positions = new Vector3[_tableSize];
            GenerateCardsPositions();
            
            _cardsOnTable = new Card[_positions.Length];
        }
        
        private void GenerateCardsPositions()
        {
            var leftPointX = -(_positions.Length * .5f - .5f) * _spacing;
            for (int i = 0; i < _positions.Length; i++)
            {
                var currentX = leftPointX + _spacing * i;
                _positions[i] = new Vector3(currentX, 0f, 0f);
            }
        }

        public bool TrySetCard(Card card)
        {
            var res = -1;

            for (int i = 0; i < _cardsOnTable.Length; i++)
            {
                if (_cardsOnTable[i] == null)
                {
                    res = i;
                    _cardsOnTable[i] = card;
                    break;
                }
            }

            if (res < 0)
                return false;
            else
            {
                StartCoroutine(MoveCardToTable(card, _positions[res], transform));
                return true;
            }
        }

        public void AnimateRotation(float time)
        {
            StartCoroutine(RotateCards(time));
        }
        
        private IEnumerator MoveCardToTable(Card card, Vector3 endPos, Transform holder)
        {
            var time = 0f;
            card.transform.SetParent(holder);
            var startPos = card.transform.localPosition;
            var startScale = card.transform.localScale;

            while (time < _movementTimer)
            {
                card.transform.localPosition = Vector3.Lerp(startPos, endPos, time/_movementTimer);
                card.transform.localScale = Vector3.Lerp(startScale, Vector3.one, time/_movementTimer);
                time += Time.deltaTime;
                yield return null;
            }

            card.transform.localPosition = endPos;
            card.transform.localScale = Vector3.one;
            card.State = CardState.OnTable;
        }

        private IEnumerator RotateCards(float timer)
        {
            var cardsToRotate = _cardsOnTable.Where(t => t != null).ToList();
            if (cardsToRotate.Count < 1) yield break;
            
            var startAngle = cardsToRotate[0].transform.localEulerAngles;
            var endAngle = new Vector3(startAngle.x, startAngle.y + 180f, startAngle.z);
            
            var time = 0f;
            while (time < timer)
            {
                foreach (var card in cardsToRotate)
                {
                    card.transform.localEulerAngles = Vector3.Lerp(startAngle, endAngle, time / timer);
                }
                
                time += Time.deltaTime;
                yield return null;
            }

            foreach (var card in cardsToRotate)
            {
                card.transform.localEulerAngles = endAngle;
            }
        }
        
    }
}
