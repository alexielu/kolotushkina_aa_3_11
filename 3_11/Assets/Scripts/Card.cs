﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards
{
    public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private GameObject _frontCard;

        [SerializeField] private MeshRenderer _picture;
        [SerializeField] private TextMeshPro _name;
        [SerializeField] private TextMeshPro _description;
        [SerializeField] private TextMeshPro _costText;
        [SerializeField] private TextMeshPro _attack;
        [SerializeField] private TextMeshPro _health;
        [SerializeField] private TextMeshPro _type;

        private bool _dragging;
        private float zPos;
        private Vector3 _lastPosition;

        private int _cost;
        public int Cost => _cost;

        [Space]
        [Header("Animation Settings")]
        [SerializeField] private Animation _animation;
        [SerializeField] private float _increasedSize;
        [SerializeField] private float _height;
        [SerializeField] private float _increaseSizeTimer;
        [SerializeField] private float _decreaseSizeTimer;
        [SerializeField] private float _returnTimer;

        public bool IsFrontSide => _frontCard.activeSelf;
        public bool Returnable { get; set; } = false;

        public CardState State { get; set; } = CardState.InDeck;

        void Start()
        {
            zPos = Camera.main.WorldToScreenPoint(transform.position).z;
        }
        
        [ContextMenu("Switch Enable")]
        public void SwitchEnable()
        {
            var state = !IsFrontSide;
            _frontCard.SetActive(state);
            _picture.enabled = state;
        }

        public void SetConfiguration(CardPropertiesData data, Material picture, string description = "")
        {
            _picture.material = picture; 
            _name.text = data.Name;
            _description.text = description;
            _costText.text = data.Cost.ToString();
            _cost = data.Cost;
            _attack.text = data.Attack.ToString();
            _health.text = data.Health.ToString();
            _type.text = data.Type == CardUnitType.None ? "" : data.Type.ToString();
        }

        public void AnimateRotation()
        {
            if (!IsFrontSide)
                _animation.Play("CardOpenRotation");
            else
                _animation.Play("CardCloseRotation");
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (State == CardState.InHand)
            {
                StartCoroutine(IncreaseCardSize());
            }
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            if (State == CardState.InHand)
            {
                StartCoroutine(DecreaseCardSize());
            }
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            if (State == CardState.InPreview)
            {
                if (Returnable)
                    GameManager.Instance.ReturnCardToDeck(this);
            }
            else if (State == CardState.InDeck)
            {
                GameManager.Instance.TryToTakeCard(this);
            }
        }
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (State == CardState.Animated) return;
            _lastPosition = transform.localPosition;
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            if (State == CardState.InHand)
                transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,zPos));
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
       
            if (Physics.Raycast(ray, out hit, 30f, LayerMask.GetMask("Playground")) && hit.collider.CompareTag("Finish"))
            {
                bool success = GameManager.Instance.TryToPutCardOnTable(this);
                if (!success)
                    StartCoroutine(ReturnCardToHand());
                else
                {
                    State = CardState.OnTable;
                }
            }
            else
            {
                StartCoroutine(ReturnCardToHand());
            }
        }

        private IEnumerator IncreaseCardSize()
        {
            var time = 0f;
            while (time < _increaseSizeTimer)
            {
                transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * _increasedSize, time/_increaseSizeTimer);
                transform.localPosition = new Vector3(transform.localPosition.x,
                    Mathf.Lerp(0f, _height, time / _increaseSizeTimer), transform.localPosition.z);
                
                time += Time.deltaTime;
                yield return null;
            }

            transform.localScale = Vector3.one * _increasedSize;
        }
        
        private IEnumerator DecreaseCardSize()
        {
            var time = 0f;
            while (time < _decreaseSizeTimer)
            {
                transform.localScale = Vector3.Lerp(Vector3.one * _increasedSize, Vector3.one, time/_increaseSizeTimer);
                transform.localPosition = new Vector3(transform.localPosition.x,
                    Mathf.Lerp(_height, 0f, time / _increaseSizeTimer), transform.localPosition.z);
                
                time += Time.deltaTime;
                yield return null;
            }
            
            transform.localScale = Vector3.one;
        }

        private IEnumerator ReturnCardToHand()
        {
            State = CardState.Animated;
            
            var startPos = transform.localPosition;
            var startScale = transform.localScale;
            var time = 0f;
            while (time < _returnTimer)
            {
                transform.localPosition = Vector3.Lerp(startPos, _lastPosition, time/_returnTimer);
                transform.localScale = Vector3.Lerp(startScale, Vector3.one, time / _returnTimer);
                time += Time.deltaTime;
                yield return null;
            }
            
            transform.localPosition = _lastPosition;
            transform.localScale = Vector3.one;
            State = CardState.InHand;
        }
    }
}