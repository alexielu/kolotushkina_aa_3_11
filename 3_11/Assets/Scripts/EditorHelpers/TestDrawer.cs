﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class TestDrawer : MonoBehaviour
    {
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawCube(transform.position, new Vector3(.7f, .01f,1f));
        }
    }
}